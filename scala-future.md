# Xử lý bất đồng bộ trong Scala 
## Đồng bộ và bất đồng bộ: 

![](async.png)

Với đồng bộ, luồng xử lý sẽ đợi cho tiến trình A xử lý hoàn tất rồi mới xử lý tiếp tiến trình B. 
Với bất đồng bộ, luồng xử lý chính sẽ chia làm 2 luồng: xử lý tiến trình A và xử lý tiến trình B luôn mà không đợi tiến trình A kết thúc.
Trong Scala Play, bất đồng bộ thường được bọc trong Future

Sử dụng bất đồng bộ giúp tăng hiệu suất, có thể thực hiện tiến trình khác trong khi chờ kết quả từ tiến trình đang chạy.

Một số trường hơp cần đồng bộ: 
- Khi cần kết quả từ tiến trình A để xử lý tiến trình B thì chúng ta sẽ phải xử lý bất đồng bộ ở tiến trình A để luồng xử lý chính xử lý tiến trình A xong mới xử lý tiếp tiến trình B.
- Khi làm việc với slick trong `Play framework` kết quả trả về thường sẽ được bọc trong Future. Do vậy, nếu muốn sử dụng kết quả để tính toán chúng ta sẽ phải đồng bộ hóa mọi tiến trình có sử dụng kết quả đấy.
## Một số cách lấy giá trị trong Future:

+ for - yield: giống for nhưng có thêm bộ đệm